<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S03: Classes and Objects</title>
	</head>
	<body>	

		<?php
           
            echo "<h1>Person</h1>";
            echo $person->printName();
            echo "<br><br>";
            
            echo "<h1>Developer</h1>";
            echo $developer->printName();
            echo "<br><br>";
            
            echo "<h1>Engineer</h1>";
            echo $engineer->printName();
            
        ?>


	</body>
</html>