<?php

// PERSON

$personData = [
    'firstName' => 'Micah',
    'middleName' => 'Angelica',
    'lastName' => 'Baclao',
];

class Person {
    protected $firstName;
    protected $middleName;
    protected $lastName;

    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName() {
        return "Your full name is {$this->firstName} {$this->middleName} {$this->lastName}.";
    }
}

$person = new Person($personData['firstName'], $personData['middleName'], $personData['lastName']);



// DEVELOPER

$developerData = [
    'firstName' => 'John',
    'middleName' => 'Finch',
    'lastName' => 'Smith',
];

class Developer extends Person {
    public function __construct($firstName, $middleName, $lastName) {
        parent::__construct($firstName, $middleName, $lastName);
    }

    public function printName() {
        return "Your name is {$this->firstName} {$this->middleName} {$this->lastName} and you are a developer.";
    }
}

$developer = new Developer($developerData['firstName'], $developerData['middleName'], $developerData['lastName']);



// ENGINEER

$engineerData = [
    'firstName' => 'Harold',
    'middleName' => 'Myers',
    'lastName' => 'Reese',
];

class Engineer extends Person {
    public function __construct($firstName, $middleName, $lastName) {
        parent::__construct($firstName, $middleName, $lastName);
    }

    public function printName() {
        return "You are an engineer named {$this->firstName} {$this->middleName} {$this->lastName}.";
    }
}

$engineer = new Engineer($engineerData['firstName'], $engineerData['middleName'], $engineerData['lastName']);

?>
