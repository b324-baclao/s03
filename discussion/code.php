<?php 


$buildingObj = (object) [
	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Hear',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

// Create an object using a Class
// It should be capitalized to indicate that it is a class
class Building {
    // Properties
    public $name;
    public $floors;
    public $address;

    // Constructor method
    // The constructor is used during the creation of an object.
    // It initializes the object's properties with the provided values.
    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // Method to print the building name
    public function printName() {
        return "The name of the building is $this->name.";
    }
}

// Instantiate the Building class and create a building object
$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City');
$secondBuilding = new Building('Trial Building', 10, 'Sa lugar kung saan di mo alam.');

//Inheritance
class Condominium extends Building{
    public $rooms;

    public function __construct($name, $floors, $address,$zipCode, $rooms){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
        $this->zipCode = $zipCode;
        $this->rooms = $rooms;
    }

    public function printName(){
        return "The name of the condominium is $this->name.";
    }

    public function checkFloors(){
        return "$this->floors with $this->rooms.";
    }

    public function checkZipCode(){
        return "The zip code is $this->zipCode";
    }


}

$condominium = new Condominium('Trial Condominium', 50, 'Manila City, Manila', '0001', 500);


//Abstraction
abstract class Drink{
    public $name;

    public function __construct($name){
        $this->name = $name;
    }

    public abstract function getDrinkName();
}

class Coffee extends Drink{
    public function getDrinkName(){
        return "The name of the coffee is $this->name.";
    }
}

$kopiko = new Coffee('Kopiko');



?>